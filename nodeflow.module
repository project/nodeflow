<?php
/**
 * @file
 * Main file for the nodeflow module.
 */

include_once('includes/nodeflow.inc');

/**
 * Implements hook_init().
 */
function nodeflow_init() {
  if (arg(0) == 'admin' && arg(1) == 'nodeflow') {
    drupal_add_library('system', 'ui.sortable');
    drupal_add_library('system', 'ui.datepicker');
    drupal_add_library('date_popup', 'timeentry');
    // Add the wvega-timepicker library if it's available.
    $wvega_path = nodeflow_get_wvega_path();
    if ($wvega_path) {
      drupal_add_js($wvega_path . '/jquery.timepicker.js');
      drupal_add_css($wvega_path . '/jquery.timepicker.css');
    }
    drupal_add_js(drupal_get_path('module', 'nodeflow') . '/scripts/nodeflow.js');
    drupal_add_css(drupal_get_path('module', 'nodeflow') . '/css/nodeflow.css');
  }
}


/**
 * Get the location of the Willington Vega timepicker library.
 */
function nodeflow_get_wvega_path() {
  $path = FALSE;
  if (function_exists('libraries_get_path')) {
    $path = libraries_get_path('wvega-timepicker');
    if (!file_exists($path)) {
      $path = FALSE;
    }
  }
  elseif (file_exists('sites/all/libraries/wvega-timepicker/jquery.timepicker.js')) {
    $path = 'sites/all/libraries/wvega-timepicker';
  }
  return $path;
}


/**
 * Implements hook_permission().
 */
function nodeflow_permission() {
  return array(
    'access nodeflow' => array(
      'title' => t('Access nodeflow'),
      'description' => t('Allow users to access and use nodeflow'),
    ),
    'administer nodeflow' => array(
      'title' => t('Administer nodeflow'),
      'description' => t('Allow users to administer nodeflow'),
    ),
  );
}


/**
 * Implements hook_menu().
 */
function nodeflow_menu() {
  $items['admin/config/system/nodeflow'] = array(
    'title' => 'Nodeflow',
    'description' => t('Configuration for nodeflow.'),
    'file' => 'nodeflow.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodeflow_admin'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/nodeflow'] = array(
    'title' => 'Nodeflow',
    'description' => 'Administer nodeflow queues',
    'page callback' => 'nodeflow_page',
    'access arguments' => array('access nodeflow'),
    'weight' => -12,
  );

  $items['admin/nodeflow/populate/%'] = array(
    'title' => 'Nodeflow populate',
    'page callback' => 'nodeflow_populate',
    'page arguments' => array(3),
    'access arguments' => array('administer nodeflow'),
    'file' => 'includes/nodeflow.config.inc',
    'type' => MENU_NORMAL_ITEM
  );

  $items['admin/nodeflow/delete/%'] = array(
    'title' => 'Nodeflow delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodeflow_delete_queue_confirm', 3),
    'access arguments' => array('administer nodeflow'),
    'file' => 'includes/nodeflow.config.inc',
    'type' => MENU_CALLBACK
  );

  $items['admin/nodeflow/%'] = array(
    'title' => 'Nodeflow',
    'page callback' => 'nodeflow_page',
    'page arguments' => array(2),
    'access arguments' => array('access nodeflow'),
  );

  $items['admin/structure/nodeflow'] = array(
    'title' => 'Nodeflows',
    'description' => 'Manage nodeflow queues',
    'page callback' => 'nodeflow_configuration_page',
    'access arguments' => array('access nodeflow'),
    'file' => 'includes/nodeflow.config.inc',
    'type' => MENU_NORMAL_ITEM
  );

  $items['admin/structure/nodeflow/add'] = array(
    'title' => 'Nodeflow add',
    'description' => 'Add a nodeflow queue',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nodeflow_manage_queues'),
    'access arguments' => array('administer nodeflow'),
    'file' => 'includes/nodeflow.config.inc',
    'type' => MENU_NORMAL_ITEM
  );

  $items['admin/nodeflow/save'] = array(
    'title' => 'Nodeflow',
    'page callback' => 'nodeflow_save',
    'access arguments' => array('access nodeflow'),
  );

  $items['admin/nodeflow/getusers'] = array(
    'title' => 'Nodeflow',
    'page callback' => 'nodeflow_get_users',
    'access arguments' => array('access nodeflow'),
  );

  return $items;
}


/**
 * Implements hook_page().
 *
 * Fetch the blocks that contain the views required
 * and set them up to display side by side.
 */
function nodeflow_page($queue_id = NULL) {
  // Find all queues in the database.
  $queues = db_select('nodeflow_queues', 'n')
            ->fields('n')
            ->orderby('weight', 'ASC')
            ->execute()
            ->fetchAll();

  if (NULL == $queue_id) {
    if (!empty($queues)) {
      $queue_id = $queues[0]->nfid;
      drupal_goto('admin/nodeflow/' . $queue_id);
      exit;
    }
    else {
      // No queues yet. Redirect to configuration page.
      drupal_goto('admin/structure/nodeflow');
      exit;
    }
  }

  // Create the pull-down menu for the queues
  $qlist = '';
  $display[1] = 'block';
  if (count($queues) > 1) {
    $qlist = '<form id="queue-list" action="" method="get">' .
             '<select class="form-select" name="queue">';
    foreach ($queues as $q) {
      $selected = ($queue_id == $q->nfid) ? 'selected="selected"' : '';
      $qlist .= '<option ' . $selected . ' value="' . url('admin/nodeflow/' . $q->nfid) . '">' . $q->name . '</option>';
      $display[$q->nfid] = $q->display;
    }
    $qlist .= '</select></form>';
  }

  // Fetch the draft view block.
  $d_block = block_load('views', 'nodeflow_draft-' . $display[$queue_id]);
  $d_render = _block_get_renderable_array(_block_render_blocks(array($d_block)));
  $d_view = render($d_render);

  // Check if nodeflow table is empty or not.
  $result = db_query("select * from {nodeflow} n left join {nodeflow_queues} q using (nfid) where n.nfid = :id", array(':id' => $queue_id))->fetchAssoc();
  if (empty($result)) {
    $p_view = t('You have no nodes available in the published view.') . '<br/>' .
              l(t('Populate the nodeflow table'), 'admin/nodeflow/populate/' . $queue_id) . '<br/>' .
              l(t('Change view settings'), 'admin/structure/views/view/nodeflow_published/edit');
  }
  else {
    $view = views_get_view('nodeflow_published');
    $view->set_arguments(array($queue_id));
    $p_view = $view->preview();
  }

  // Add markup to allow showing online users.
  $ulist = '<div class="users-online"><b>';
  $ulist .= t('Users currently online');
  $ulist .= '</b>: <ul></ul></div>';

  // Join them together in a side-by-side display.
  $markup = $ulist . $qlist .
  	    '<div class="published-view-block">' . $p_view . '</div>' .
            '<div class="draft-view-block">' . $d_view . '</div>' .
            '<div class="clear-left"></div>';

  return array('#markup' => $markup);
}


/**
 * Show nodeflow online users that we stash in cache.
 */
function nodeflow_get_users() {
  $cid = 'nodeflow:users';
  $users = array();
  global $user;

  // Fetch the cache object.
  $cacheobj = cache_get($cid, 'cache');

  if ($cacheobj->data) {
    $users = $cacheobj->data;
  }
  $users[$user->name] = time();

  // All users with timestamp over half a minute are removed.
  foreach ($users as $usr => $timestamp) {
    if ((time() - $timestamp) > 30) {
      unset($users[$usr]);
    }
  }

  // Create cache entry.
  cache_set($cid, $users, 'cache');

  return drupal_json_output($users);
}


/**
 * Implements hook_views_api().
 */
function nodeflow_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'nodeflow') . '/views',
  );
}


/**
 * Create a form to embed the nodeflow view in it.
 */
function nodeflow_form($form, &$form_state) {
  $form['nid'] = array(
    '#type' => 'hidden',
  );
  $form['qid'] = array(
    '#type' => 'hidden',
    '#value' => arg(2),
  );
  $form['locked'] = array(
    '#type' => 'hidden',
  );
  $form['future_position'] = array(
    '#type' => 'hidden',
  );
  $form['schedule_date'] = array(
    '#type' => 'hidden',
  );
  $form['schedule_time'] = array(
    '#type' => 'hidden',
  );
  $form['draft'] = array(
    '#type' => 'hidden',
  );
  $form['view_embed'] = array(
    '#markup' => '[nodeflow_view_embed]',
  );
  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/nodeflow', array(
      'attributes' => array(
        'class' => array('button'),
      ),
    )),
  );
  $form['#action'] = 'save';
  return $form;
}


/**
 * Custom form submit handler to redirect page away from the default
 * nodeflow page handler. Form submission handling is taken care of
 * by the nodeflow_form_submit().
 */
function nodeflow_save() {
  // Re-generate form for drupal form handler to work.
  $nodeflow_form = drupal_get_form('nodeflow_form');
}


/**
 * Implements hook_form_submit().
 *
 * @TODO queue_id to be validated by the relevant class (to be written)
 */
function nodeflow_form_submit($form, &$form_state) {
  $queue_id = $form_state['input']['qid'];
  $queue_id = filter_var($queue_id, FILTER_VALIDATE_INT);
  if (empty($queue_id)) {
    drupal_set_message(t('Invalid queue ID passed'), 'error');
    exit;
  }
  
  // To find out which nodes have been deleted from the queue, we need to see what the queue looks like before saving.
  $removed_nodes = array();
  foreach (nodeflow_fetch_queue($queue_id) as $nid => $row) {
    $removed_nodes[$nid] = $row;
  }

  // Restack the array to make it easier to insert rows.
  $fv = array();
  $position = 0;
  foreach ($form_state['values']['nid'] as $key => $val) {

    // Node is present, so remove it from removed_nodes array
    unset($removed_nodes[$val]);
    
    $fv[$val]['nid'] = $val;
    $fv[$val]['locked'] = isset($form_state['values']['locked'][$key]) ? $form_state['values']['locked'][$key] : 0;

    if (!empty($form_state['values']['schedule_date'][$val])) {
      // Should probably do some better data validation here.
      $publish_time = !empty($form_state['values']['schedule_time'][$val]) ? $form_state['values']['schedule_time'][$val] : '00:00';
      $publish_date = $form_state['values']['schedule_date'][$val] . ' ' . $publish_time . ':00';
      $publish_on = strtotime($publish_date);

      $fv[$val]['publish_on'] = $publish_on;

      // If article is scheduled for publishing in the future, remove its current position (and thusly from the queue).
      // This will allow already published articles to be scheduled to appear in the queue at a set time point.
      // But this will not work for multiple lists .. @TODO: implement timelining
      $fv[$val]['future_position'] = $position;
      $fv[$val]['position'] = NULL;
    } else {
      // Save existing future position value
      if (isset($form_state['values']['future_position'][$key]) && $form_state['values']['future_position'][$key] != '') {
        $fv[$val]['position'] = NULL;
        $fv[$val]['future_position'] = $position;
      } else {
        $fv[$val]['position'] = $position;
        $fv[$val]['future_position'] = NULL;
        // Only increment position counter if position is actually in use.
        $position++;
      }
    }
  }
  
  // Action any removed nodes
  if (!empty($removed_nodes)) {
    foreach ($removed_nodes as $nid => $row) {
      // We assume that any node being removed and has a scheduler setting, it was added by nodeflow.
      //
      // @README Potential issue when the node is scheduled to be published on more than one queue.
      // That queue will need to have the row manually removed, otherwise it will just remain there in limbo.
      // Doing this automatically is possible but will require more queries, and we are not sure how often
      // this scenario will be executed.  If it is causing a problem, then we can add it :)
      // In the future we plan to implement "timelining" (how a queue will look will be timestamped),
      // which will allow nodeflow to handle the display of articles in a queue more natively.
      db_delete('scheduler')
        ->condition('nid', $nid)
        ->execute();
    }
  }

  // Publish draft nodes or save scheduled time for publishing later (via scheduler).
  if (!empty($form_state['values']['draft'])) {
    foreach ($form_state['values']['draft'] as $key => $nid) {
      if (!empty($fv[$nid]['publish_on'])) {

        db_merge('scheduler')
          ->key(array('nid' => $nid))
          ->fields(array('publish_on' => $fv[$nid]['publish_on']))
          ->execute();

      } else {
        // Node is in draft status, so publish it.
        $node = node_load($nid);
        $node->status = 1;
        // Set created time to published time.
        if (variable_get('nodeflow_update_created_time', '0')) {
          $node->created = NULL;
        }
        try {
          node_save($node);
        }
        catch (Exception $e) {
          drupal_set_message(t('Error updating node\'s status'), 'error', FALSE);
          drupal_goto('admin/nodeflow/' . $queue_id);
        }
      }
    }
  }

  // Save records to the nodeflow table.
  $records = new Nodeflow\NodeflowQueue;
  foreach ($fv as $pos => $row) {
    // Make sure id is available.
    if (!empty($row['nid'])) {
      // Create new nodeflow order record.
      $locked = isset($row['locked']) ? $row['locked'] : 0;
      //$records[] = new \Nodeflow\Db\Nodeflow($queue_id, $row['nid'], $row['position'], $locked, $row['future_position']);
      $records->insert(new \Nodeflow\Db\Nodeflow($queue_id, $row['nid'], $row['position'], $locked, $row['future_position']));
    }
  }

  if (!empty($records)) {
    $records->build();
    nodeflow_insert_all_records($records);
  }

  drupal_set_message(t('The changes have been saved.'), 'status', FALSE);

  // Redirect to the nodeflow index function.
  drupal_goto('admin/nodeflow/' . $queue_id);
}


/**
 * Hook to the table view to wrap the form around the rows.
 */
function nodeflow_preprocess_views_view(&$vars) {
  if ($vars['view']->name == 'nodeflow_published') {
    $view_form = drupal_get_form('nodeflow_form');
    $view_form['view_embed']['#markup'] = $vars['rows'];
    $vars['rows'] = $view_form;
  }
}


/**
 * Hook to the table view to modify the rows and add our nodeflow data to it.
 */
function nodeflow_preprocess_views_view_table(&$vars) {
  switch ($vars['view']->name) {
    case 'nodeflow_published':
      nodeflow_preprocess_published_table($vars);
    break;

    case 'nodeflow_draft':
      nodeflow_preprocess_draft_table($vars);
    break;
  }
}

/**
 * hook_views_query_alter().
 *
 * Order nodeflow published view (admin overview) correctly according to future positions.
 */
function nodeflow_views_query_alter(&$view, &$query) {
  if ($view->name == 'nodeflow_published') {
    $view->query->orderby[0]['field'] = 'if(future_position, future_position, position), position';
  }
}


/**
 * Preprocess published views table.
 */
function nodeflow_preprocess_published_table(&$vars) {
  // Fetch node position lock information.
  $queue = nodeflow_fetch_queue($vars['view']->args[0]);
  $imgpath = drupal_get_path('module', 'nodeflow') . '/images';

  foreach ($vars['rows'] as $key => $val) {
    // Replace the nid with the position number.
    $nid = $val['nid'];
    $row = $queue[$nid];

    if (empty($row)) {
      drupal_set_message('Couldn\'t find row information in NodeflowQueue. NID: ' . $nid, 'error');
    }

    $lock = $row->locked;
    if ($lock == 1) {
      $vars['row_classes'][$key][] = 'locked';
    }
    $vars['row_classes'][$key][] = 'nid' . $nid;

    $position = $row->position;
    if ($vars['rows'][$key]['status'] == t('No') || !isset($position)) {
      $vars['row_classes'][$key][] = 'scheduled';
    }
    unset($vars['rows'][$key]['status']);

    $vars['rows'][$key]['nid']  = '<a href="#" class="tabledrag-handle" title="' . t('Drag and drop') . '"><div class="handle"></div></a>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" name="locked[]" value="' . $lock . '" id="poslock' . $key . '"/>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" name="future_position[]" value="' . $row->future_position . '" id="future_pos' . $key . '"/>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" class="nid" name="nid[]" value="' . $nid . '"/>';
    $vars['rows'][$key]['nid'] .= '<div class="counter"></div>';

    if (user_access('administer nodes')) {
      $vars['rows'][$key]['operations'] = '<div class="edit icon" title="' . t('Edit node') . '">' . l('', 'node/' . $nid . '/edit') . '</div>';
    } else {
      $vars['rows'][$key]['operations'] = '';
    }
    $vars['rows'][$key]['operations'] .= '<div class="lock icon" id="lock' . $key . '"></div>';
    $vars['rows'][$key]['operations'] .= '<div class="remove icon"></div>';

    $vars['field_classes']['nid'][$key] = 'views-field views-field-handle';
    $vars['field_classes']['operations'][$key] = 'views-field views-field-operations';

    $vars['field_attributes']['nid'][$key] = array();
    $vars['field_attributes']['operations'][$key] = array();
  }

  unset($vars['header']['status']);
  $vars['header']['nid'] = '';
  $vars['header']['operations'] = 'Operations';
  $vars['header_classes']['nid'] = 'views-field views-field-handle';
  $vars['header_classes']['operations'] = 'views-field views-field-operations';
  $vars['attributes_array']['id'] = 'nodeflow-published';
}


/**
 * Preprocess draft views table.
 */
function nodeflow_preprocess_draft_table(&$vars) {
  foreach ($vars['rows'] as $key => $val) {
    $vars['rows'][$key]['nid']  = '<a href="#" class="tabledrag-handle" title="' . t('Drag and drop') . '"><div class="handle"></div></a>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" name="nid[]" value="' . $val['nid'] . '"/>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" name="locked[]" value="0" id="poslock' . $key . '"/>';
    $vars['rows'][$key]['nid'] .= '<input type="hidden" name="future_position[]" value="" id="future_pos' . $key . '"/>';
    // Remove the draft value if node is published.
    if ($val['status'] == t('No')) {
      $vars['rows'][$key]['nid'] .= '<input type="hidden" name="draft[]" value="' . $val['nid'] . '"/>';
    }
    $vars['rows'][$key]['nid'] .= '<div class="counter"></div>';

    $pubdate = $pubtime = '';
    if (!empty($vars['rows'][$key]['publish_on'])) {
      $pub = explode(' ', $vars['rows'][$key]['publish_on']);
      $pubdate = $pub[0];
      $pubtime = $pub[1];
    }
    $vars['rows'][$key]['publish_on']  = '<input type="text" class="schedule_date" name="schedule_date[' . $val['nid'] . ']" value="' . $pubdate . '"/>';
    $vars['rows'][$key]['publish_on'] .= '<input type="text" class="schedule_time" name="schedule_time[' . $val['nid'] . ']" value="' . $pubtime . '"/>';

    $vars['row_classes'][$key][] = 'draft';
    $vars['field_classes']['nid'][$key] = 'views-field views-field-handle';
    $vars['field_attributes']['nid'][$key] = array();
  }

  $vars['header']['nid'] = '';
  $vars['header_classes']['nid'] = 'views-field views-field-handle';
  $vars['attributes_array']['id'] = 'nodeflow-draft';
}


/**
 * Fetch nodeflow table.
 *
 * @param int Nodeflow queue ID
 * @return array
 * @TODO Move to relevant class
 */
function nodeflow_fetch_queue($queue_id = 0) {
  $q = db_query("SELECT * FROM {nodeflow} where nfid = :queue_id ORDER BY position ASC", array(':queue_id' => abs((int) $queue_id)));
  //$nodeflow = array();
  $nodeflow = new \Nodeflow\NodeflowQueue;
  if (!empty($q)) {
    foreach ($q as $n) {
      $nodeflow->insert(new \Nodeflow\Db\Nodeflow($n->nfid, $n->nid, $n->position, $n->locked, $n->future_position));
    }
    $nodeflow->build();
  }
  return $nodeflow;
}


/**
 * Insert records into the nodeflow table.
 * Currently only supports one queue insert at a time.
 */
function nodeflow_insert_all_records(\Nodeflow\NodeflowQueue $records) {
  try {
    // @TODO check if $records contains a ready built queue or not

    $transaction = db_transaction();

    $nodes = array();
    foreach ($records as $record) {
      $record->save();
      $nodes[] = $record->nid;

      // Get the queue ID for this insert.
      if (empty($queue_id)) {
        $queue_id = $record->nfid;
      }
    }

    // Table cleanup ... remove any unneeded records.
    db_delete('nodeflow')
      ->condition('nfid', $queue_id)
      // position column counts from 0
      ->condition('nid', $nodes, 'not in')
      ->execute();

    unset($transaction);

    // Enables rules to add actions based on this save event.
    if (module_exists('rules')) {
      rules_invoke_event('nodeflow_save', $queue_id);
    }

  } catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message(t('Error saving nodeflow changes: @error', array('@error' => $e->getMessage())), 'error');
  }
}


/**
 * Insert a node into the nodeflow queue as the requested position.  If moving into a locked position,
 * the locked position will lose its place and shuffle down the queue with the other rows.
 *
 * @param object node
 * @param int nodeflow queue ID
 * @param int position in queue to insert the node
 * @see nodeflow.rules.inc
 * @TODO Move to relevant class
 */
function nodeflow_insert_into_queue($node, $queue_id = 0, $position = 0) {

  // check $node is a node allowed by nodeflow?

  $nodeflow = nodeflow_fetch_queue($queue_id);
  $nodeflow->insert(new \Nodeflow\Db\Nodeflow($queue_id, $node->nid, $position, 0));
  $nodeflow->build();

  nodeflow_insert_all_records($nodeflow);
}

/**
 * hook_scheduler_api();
 *
 * When scheduler is run we need to update the nodeflow queue and move all relevant nodes into their
 * correct position.
 */
function nodeflow_scheduler_api($node, $action) {
  if ('publish' == $action) {
    // Get a list of all nodeflow queues this node is attached too.  There should only ever be one
    // node in any one queue.
    // Time publishing the article in to different queues at different times will not (currently) work.
    $q = db_query("SELECT DISTINCT nfid as nfid FROM {nodeflow} where nid = :nid and future_position is not null ORDER BY position ASC", array(':nid' => $node->nid));
    if (!empty($q)) {
      // There should o
      foreach ($q as $n) {
        $nodeflow = nodeflow_fetch_queue($n->nfid);

        $row = $nodeflow[$node->nid];
        $nodeflow->moveIntoQueue($row);
        $nodeflow->build();

        nodeflow_insert_all_records($nodeflow);
      }
    }
  }
}

/**
 * Implements hook_node_update().
 */
function nodeflow_node_update($node) {
  // If workbench module is installed we need a different approach.
  if (module_exists('workbench_moderation')) {
    // @todo: Find a neat approach to figure out unpublished node.
  }
  else {
    if ($node->status == 0 && $node->original->status == 1) {
      $q = db_query("SELECT DISTINCT nfid as nfid FROM {nodeflow} where nid = :nid ORDER BY position ASC", array(':nid' => $node->nid));
      if (!empty($q)) {
        foreach ($q as $n) {
          $nodeflow = nodeflow_fetch_queue($n->nfid);

          $nodeflow->remove($node->nid);
          $nodeflow->build();

          nodeflow_insert_all_records($nodeflow);
        }
      }
    }
  }
}

/**
 * Rules action: Add a nodeflow queue.
 */
function nodeflow_rules_add_queue($name = '', $weight = 0) {

  $queue = new \Nodeflow\Db\NodeflowQueue(t($name), $weight);
  $queue_id = $queue->save();

  if ($queue_id) {
    return array('nodequeue_id_created' => $queue_id);
  }
  else {
    return array();
  }
}
