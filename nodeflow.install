<?php
/**
 * @file
 * Install function for the nodeflow module.
 */

/**
 * Implements hook_install().
 */
function nodeflow_install() {
  $t = get_t();
  drupal_set_message($t('The nodeflow module has been successfully installed and the nodeflow page has been added to the administration menu.'));
}

/**
 * Implements hook_uninstall().
 */
function nodeflow_uninstall() {
  db_drop_table('nodeflow');
  db_drop_table('nodeflow_queues');
  
  include('views/nodeflow.views_default.inc');
  foreach (array_keys(nodeflow_views_default_views()) as $name) {
    $view = views_ui_cache_load($name);
    if ($view) {
      $view->delete();
    }
  }
}

function nodeflow_update_7001() {
  db_query("alter table nodeflow drop index nfid;");
  db_query("alter table nodeflow change nfid pid int unsigned not null auto_increment;");
  db_query("alter table nodeflow add nfid int not null default '0' after pid;");
  db_query("alter table nodeflow add index(nfid);");
  db_query("alter table nodeflow add unique node_q (nfid, nid);");
  
  db_query("CREATE TABLE `nodeflow_queues` (`nfid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Nodeflow queue ID.', `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'Nodeflow queue name.', PRIMARY KEY (`nfid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ID numbers and name identifier to Nodeflow queues'");
  
  db_query("insert into nodeflow_queues (name) values ('Frontpage main');");
  db_query("update nodeflow set nfid = 1;");
}

function nodeflow_update_7002() {
  db_query("alter table nodeflow_queues add weight tinyint default'0'");
}

function nodeflow_update_7003() {
  db_query("alter table nodeflow add future_position tinyint null default null;");
  db_query("alter table nodeflow modify position tinyint null default null;");

  
  $view = views_get_view('nodeflow_draft', TRUE); 
  $view->display['block']->display_options['fields']['status']['id'] = 'status';
  $view->display['block']->display_options['fields']['status']['table'] = 'node';
  $view->display['block']->display_options['fields']['status']['field'] = 'status';
  $view->display['block']->display_options['fields']['status']['element_label_colon'] = FALSE;
  $view->display['block']->display_options['fields']['status']['not'] = 0;
  views_save_view($view);
}

/**
 * Update position/weight columns type from tinyint to int.
 */
function nodeflow_update_7004() {
  db_query("alter table nodeflow modify position int default NULL;");
  db_query("alter table nodeflow modify future_position int default NULL;");
  db_query("alter table nodeflow_queues modify weight int DEFAULT '0'");
}

/**
 * Update nodeflow queue table to add display column.
 */
function nodeflow_update_7005() {
  db_query("alter table nodeflow_queues add column display varchar(64) NOT NULL default 'block'");
}


/**
 * Implements hook_schema().
 */
function nodeflow_schema() {

  $schema['nodeflow'] = array(
    'description' => 'Stores nodeflow data.',
    'fields' => array(
      'pid'  => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Primary Key: Unique nodeflow ID.',
      ),
      'nfid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "Nodeflow queue ID",
      ),
      'nid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => "Node ID",
      ),
      'locked' => array(
        'type' => 'int',
        'size' => 'tiny',
        'precision' => 1,
        'not null' => TRUE,
        'default' => 0,
        'description' => "Lock boolean",
      ),
      'position' => array(
        'type' => 'int',
        'default' => NULL,
        'description' => "Position of the node",
      ),
      'future_position' => array(
        'type' => 'int',
        'default' => NULL,
        'description' => "Future position of the node",
      )
    ),
    'primary key' => array('pid'),
    'unique' => array(
      'node_q' => array('nfid', 'nid')
    ),
    'indexes' => array(
      'nid'    => array('nid')
    ),
  );
  
  $schema['nodeflow_queues'] = array(
    'description' => 'ID numbers and name identifier to Nodeflow queues',
    'fields' => array(
      'nfid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Primary Key: Nodeflow queue ID.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => "Nodeflow queue name.",
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => FALSE,
        'default' => '0',
        'description' => "Nodeflow queue weight.",
      ),
      'display' => array(
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => 'block',
        'description' => "Nodeflow display name.",
      ),
    ),
    'primary key' => array('nfid')
  );

  return $schema;
}
